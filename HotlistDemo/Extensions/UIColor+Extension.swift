//
//  UIColor+Extension.swift
//  HotlistDemo
//
//  Created by Nikita Entin on 29.08.2023.
//

import UIKit

extension UIColor {
    
    static let coldGray900 = UIColor(r: 19, g: 23, b: 34)
    static let coldGray100 = UIColor(r: 240, g: 243, b: 250)
    static let coldGray150 = UIColor(r: 224, g: 227, b: 235)
}

private extension UIColor {
    
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(red: r / 255, green: g / 255, blue: b / 255, alpha: 1.0)
    }
    
}
