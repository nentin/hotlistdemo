//
//  CollectionCellController.swift
//  HotlistDemo
//
//  Created by Nikita Entin on 29.08.2023.
//

import UIKit

protocol CollectionCellControllerDelegate: AnyObject {
    
    func didSelectItem(at indexPath: IndexPath)
    
}

final class CollectionCellController: UIViewController {
    
    weak var delegate: CollectionCellControllerDelegate?
    
    private lazy var collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
    
    private var items: [HotlistItem] = []
    
    private var isCompact: Bool {
        return traitCollection.horizontalSizeClass == .compact && traitCollection.verticalSizeClass == .regular
    }
    
    private var showSkeleton = true
    
    private let layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = .zero
        layout.minimumInteritemSpacing = .zero
        layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        return layout
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(collection)
        collection.showsHorizontalScrollIndicator = false
        collection.delegate = self
        collection.dataSource = self
        collection.register(RegularHotlistCell.self, forCellWithReuseIdentifier:.regular)
        collection.register(LargeHotlistCell.self, forCellWithReuseIdentifier: .large)
        collection.register(HotlistSkeletonCell.self, forCellWithReuseIdentifier: .skeleton)
        collection.isScrollEnabled = false

    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate { [weak self] _ in
            guard let self else { return }
            self.collection.collectionViewLayout.invalidateLayout()
            self.updateScrolling()
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collection.frame = view.bounds
    }
    
    func updateState(with items: [HotlistItem]) {
        guard self.items != items else { return }
        showSkeleton = false
        self.items = items
        updateScrolling()
        collection.reloadData()
    }
    
    private func updateScrolling() {
        collection.isScrollEnabled = isCompact
    }
    
    private func calculateRowWidth(using width: CGFloat) -> CGFloat {
        let resultWidth: CGFloat
        if isCompact {
            resultWidth = width - 2 * 15 - 18
        } else {
            resultWidth = (width / 2) - 2 * 16
        }
        return resultWidth
    }
    
}

extension CollectionCellController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = calculateRowWidth(using: collectionView.frame.width)
        let height = 2 * collectionView.bounds.height / CGFloat(collectionView.numberOfItems(inSection: 0))
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        delegate?.didSelectItem(at: indexPath)
    }
    
}

extension CollectionCellController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return showSkeleton ? 6 : items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if showSkeleton {
            return collectionView.dequeueReusableCell(withReuseIdentifier: .skeleton, for: indexPath)
        } else {
            let item = items[indexPath.item]
            if item.marketCap == nil {
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: .regular, for: indexPath) as? RegularHotlistCell else {
                    fatalError()
                }
                cell.update(item: item)
                return cell
            } else {
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: .large, for: indexPath) as? LargeHotlistCell else {
                    fatalError()
                }
                cell.update(item: item)
                return cell
            }
        }
    }
    
}

private extension String {
    
    static let skeleton = String(describing: HotlistSkeletonCell.self)
    static let regular = String(describing: RegularHotlistCell.self)
    static let large = String(describing: LargeHotlistCell.self)
    
}
