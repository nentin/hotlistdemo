//
//  HotlistCell.swift
//  HotlistDemo
//
//  Created by Nikita Entin on 29.08.2023.
//

import UIKit

final class RegularHotlistCell: UICollectionViewCell {
    
    private let imageView = UIImageView()
    private let symbolLabel = UILabel()
    private let marketStatusView = MarketStatusView()
    private let priceView = HotlistPriceVIew()
    private let tickerView = HotlistTickerView()
    private let priceChangeView = PriceChangeView()
    private let separatorView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupInitialState()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(item: HotlistItem) {
        imageView.image = UIImage(named: item.logo ?? "")
        symbolLabel.text = item.symbol
        marketStatusView.update(with: item.premarketStatus ?? [])
        priceView.update(with: item.price, currency: "USD")
        tickerView.update(with: item.ticker ?? "")
        priceChangeView.update(with: item.priceChange)
        setNeedsLayout()
    }
    
    private func setupInitialState() {
        setupImageView()
        setupSymbolLabel()
        setupMarketStatusView()
        setupPriceView()
        setupTickerView()
        setupPriceChangeView()
        setupSeparatorView()
    }
    
    private func setupImageView() {
        contentView.addSubview(imageView)
        imageView.layer.cornerRadius = .imageViewCornerRadius
    }
    
    private func setupSymbolLabel() {
        contentView.addSubview(symbolLabel)
        symbolLabel.numberOfLines = 1
        symbolLabel.font = .preferredFont(forTextStyle: .headline)
        symbolLabel.textColor = .coldGray900
    }
    
    private func setupMarketStatusView() {
        contentView.addSubview(marketStatusView)
    }
    
    private func setupPriceView() {
        contentView.addSubview(priceView)
    }
    
    private func setupTickerView() {
        contentView.addSubview(tickerView)
        tickerView.clipsToBounds = true
        tickerView.layer.cornerRadius = .cornerRadius
    }
    
    private func setupPriceChangeView() {
        contentView.addSubview(priceChangeView)
    }
    
    private func setupSeparatorView() {
        contentView.addSubview(separatorView)
        separatorView.backgroundColor = .coldGray150
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layoutImageView()
        layoutFirstLine()
        layoutSecondLine()
        layoutSeparatorView()
    }
    
    private func layoutImageView() {
        let size = imageView.image?.size ?? .zero
        let origin = CGPoint(x: .leadingInset, y: (contentView.bounds.height - size.height) / 2)
        imageView.frame = CGRect(origin: origin, size: size)
    }
    
    private func layoutFirstLine() {
        let symbolLabelMinWidth = symbolLabel.text?.firstFourCharactersWithDotsWidth ?? .zero
        let premarketsViewSize = marketStatusView.sizeThatFits(contentView.bounds.size)
        let priceAvailableWidth = contentView.bounds.width
        - imageView.frame.maxX
        - .imageTrailingInset - premarketsViewSize.width
        - symbolLabelMinWidth
        - .priceLeadingInset
        let priceSize = priceView.sizeThatFits(CGSize(width: priceAvailableWidth, height: bounds.height))
        let minTrailingWidth = min(priceAvailableWidth, priceSize.width)
        priceView.frame = CGRect(
            x: contentView.bounds.width - minTrailingWidth,
            y: .topInset,
            width: min(priceAvailableWidth, priceSize.width),
            height: priceSize.height
        )
        let symbolAvailableWidth = priceView.frame.minX
        - premarketsViewSize.width
        - imageView.frame.maxX
        - .imageTrailingInset
        - .priceLeadingInset
        let symbolSize = symbolLabel.sizeThatFits(contentView.bounds.size)
        symbolLabel.frame = CGRect(
            x: imageView.frame.maxX + .imageTrailingInset,
            y: .topInset,
            width: min(max(symbolAvailableWidth, symbolLabelMinWidth), symbolSize.width),
            height: symbolSize.height
        )
        
        marketStatusView.frame = CGRect(
            x: symbolLabel.frame.maxX + .symbolTrailingInset,
            y: symbolLabel.frame.midY - premarketsViewSize.height / 2,
            width: premarketsViewSize.width,
            height: premarketsViewSize.height
        )
    }
    
    private func layoutSecondLine() {
        let tickerSize = tickerView.sizeThatFits(contentView.bounds.size)
        tickerView.frame = CGRect(
            x: imageView.frame.maxX + .imageTrailingInset,
            y: symbolLabel.frame.maxY + .interLineInset,
            width: tickerSize.width,
            height: tickerSize.height
        )
        
        let priceChangeSize = priceChangeView.sizeThatFits(contentView.bounds.size)
        priceChangeView.frame = CGRect(
            x: contentView.bounds.width - priceChangeSize.width,
            y: priceView.frame.maxY + .interLineInset,
            width: priceChangeSize.width,
            height: priceChangeSize.height
        )
    }
    
    private func layoutSeparatorView() {
        let size = CGSize(width: contentView.bounds.width - .leadingInset, height: .separatorHeight)
        let origin = CGPoint(x: .leadingInset, y: contentView.bounds.height - size.height)
        separatorView.frame = CGRect(origin: origin, size: size)
    }
    
}

private extension String {
    
    var firstFourCharactersWithDotsWidth: CGFloat {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.preferredFont(forTextStyle: .headline)]
        let width = self.size(withAttributes: attrs).width
        let prefixedStringWidth = String(prefix(4) + "…").size(withAttributes: attrs).width
        return min(prefixedStringWidth, width)
    }
    
}

private extension CGFloat {
    
    static let cornerRadius: CGFloat = 4
    static let imageViewCornerRadius: CGFloat = 18
    static let leadingInset: CGFloat = 16
    static let imageTrailingInset: CGFloat = 12
    static let priceLeadingInset: CGFloat = 8
    static let topInset: CGFloat = 8
    static let interLineInset: CGFloat = 2
    static let separatorHeight: CGFloat = 0.5
    static let symbolTrailingInset: CGFloat = 4
    
}
