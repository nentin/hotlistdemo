//
//  HotlistSkeletonCell.swift
//  HotlistDemo
//
//  Created by Nikita Entin on 31.08.2023.
//

import UIKit

final class HotlistSkeletonCell: UICollectionViewCell {
    
    private let logoView = UIView()
    private let topLeadingView = UIView()
    private let topTrailingView = UIView()
    private let bottomLeadingView = UIView()
    private let bottomTrailingView = UIView()
    
    private let separatorView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        setupInitialState()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        logoView.frame = CGRect(origin: CGPoint(x: .leadingInset, y: .logoTopInset), size: .logoSize)
        
        let topLeadingViewOrigin = CGPoint(x: logoView.frame.maxX + .logoTrailingInset, y: .topViewTopInset)
        topLeadingView.frame = CGRect(origin: topLeadingViewOrigin, size: .topLeadingViewSize)
        
        let topTrailingViewOrigin = CGPoint(
            x: contentView.bounds.width - CGSize.topTrailingViewSize.width,
            y: .topViewTopInset
        )
        topTrailingView.frame = CGRect(origin: topTrailingViewOrigin, size: .topTrailingViewSize)
        
        let bottomLeadingViewOrigin = CGPoint(
            x: logoView.frame.maxX + .logoTrailingInset,
            y: topLeadingView.frame.maxY + .interLineInset
        )
        bottomLeadingView.frame = CGRect(origin: bottomLeadingViewOrigin, size: .bottomLeadingViewSize)
        
        let bottomTrailingViewOrigin = CGPoint(
            x: contentView.bounds.width - CGSize.bottomTrailingViewSize.width,
            y: topTrailingView.frame.maxY + .interLineInset
        )
        bottomTrailingView.frame = CGRect(origin: bottomTrailingViewOrigin, size: .bottomTrailingViewSize)
        
        let separatorSize = CGSize(width: contentView.bounds.width - .leadingInset, height: .separatorHeight)
        let origin = CGPoint(x: .leadingInset, y: contentView.bounds.height - separatorSize.height)
        separatorView.frame = CGRect(origin: origin, size: separatorSize)
    }
    
    private func setupInitialState() {
        contentView.addSubview(logoView)
        contentView.addSubview(topLeadingView)
        contentView.addSubview(topTrailingView)
        contentView.addSubview(bottomLeadingView)
        contentView.addSubview(bottomTrailingView)
        contentView.addSubview(separatorView)
        
        logoView.backgroundColor = .coldGray100
        topLeadingView.backgroundColor = .coldGray100
        topTrailingView.backgroundColor = .coldGray100
        bottomLeadingView.backgroundColor = .coldGray100
        bottomTrailingView.backgroundColor = .coldGray100
        separatorView.backgroundColor = .coldGray150
        
        logoView.clipsToBounds = true
        topLeadingView.clipsToBounds = true
        topTrailingView.clipsToBounds = true
        bottomLeadingView.clipsToBounds = true
        bottomTrailingView.clipsToBounds = true
        
        logoView.layer.cornerRadius = CGSize.logoSize.width / 2
        topLeadingView.layer.cornerRadius = 4
        topTrailingView.layer.cornerRadius = 4
        bottomLeadingView.layer.cornerRadius = 4
        bottomTrailingView.layer.cornerRadius = 4
    }
    
}

private extension CGFloat {
    
    static let leadingInset: CGFloat = 16
    static let logoTopInset: CGFloat = 12
    static let logoTrailingInset: CGFloat = 12
    static let topViewTopInset: CGFloat = 8
    static let interLineInset: CGFloat = 4
    static let separatorHeight: CGFloat = 0.5
    
}

private extension CGSize {
    
    static let logoSize = CGSize(width: 36, height: 36)
    static let topLeadingViewSize = CGSize(width: 120, height: 20)
    static let topTrailingViewSize = CGSize(width: 80, height: 20)
    static let bottomLeadingViewSize = CGSize(width: 48, height: 20)
    static let bottomTrailingViewSize = CGSize(width: 56, height: 20)
    
}
