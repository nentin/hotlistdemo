//
//  HotlistPriceVIew.swift
//  HotlistDemo
//
//  Created by Nikita Entin on 31.08.2023.
//

import UIKit

final class HotlistPriceVIew: UIView {
    
    private let priceLabel = UILabel()
    private let currencyLabel = UILabel()
    
    private let priceSkeltonView = UIView()
    
    private var extraInset: CGFloat {
        return currencyLabel.text?.isEmpty == false ? 3 : .zero
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupInitialState()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(with price: String?, currency: String?) {
        if price?.isEmpty == false {
            priceLabel.text = price
            currencyLabel.text = currency
            priceSkeltonView.isHidden = true
            priceLabel.isHidden = false
            currencyLabel.isHidden = false
        } else {
            priceLabel.isHidden = true
            currencyLabel.isHidden = true
            priceSkeltonView.isHidden = false
        }
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        if priceLabel.text == nil {
            return .skeleton
        } else {
            let priceSize = priceLabel.sizeThatFits(size)
            let currencySize = currencyLabel.sizeThatFits(size)
            return CGSize(width: priceSize.width + currencySize.width + extraInset, height: priceSize.height)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let priceSize = priceLabel.sizeThatFits(bounds.size)
        let currencySize = currencyLabel.sizeThatFits(bounds.size)
        let neededSize = CGSize(
            width: min(bounds.width - currencySize.width - extraInset, priceSize.width),
            height: priceSize.height
        )
        priceLabel.frame = CGRect(origin: .zero, size: neededSize)
        
        let currencyOrigin = CGPoint(x: priceLabel.frame.maxX + extraInset, y: priceLabel.frame.midY - currencySize.height / 2)
        currencyLabel.frame = CGRect(origin: currencyOrigin, size: currencySize)
        
        priceSkeltonView.frame = CGRect(origin: .zero, size: .skeleton)
    }
    
    private func setupInitialState() {
        addSubview(priceLabel)
        addSubview(currencyLabel)
        addSubview(priceSkeltonView)
        priceSkeltonView.isHidden = true
        priceSkeltonView.backgroundColor = .coldGray100
        priceSkeltonView.clipsToBounds = true
        priceSkeltonView.layer.cornerRadius = 4
        
        priceLabel.numberOfLines = 1
        currencyLabel.numberOfLines = 1
        currencyLabel.font = .preferredFont(forTextStyle: .caption2)
        priceLabel.font = .preferredFont(forTextStyle: .subheadline)
    }
    
}

private extension CGSize {
    
    static let skeleton = CGSize(width: 80, height: 20)
    
}
