//
//  HotlistTickerView.swift
//  HotlistDemo
//
//  Created by Nikita Entin on 29.08.2023.
//

import UIKit

final class HotlistTickerView: UIView {
    
    private let backgroundView = UIView()
    private let textLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        addSubview(backgroundView)
        backgroundView.addSubview(textLabel)
        
        backgroundView.backgroundColor = .coldGray100
        textLabel.font = .preferredFont(forTextStyle: .caption1)
        textLabel.textColor = .coldGray900
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let textSize = textLabel.sizeThatFits(size)
        return CGSize(width: textSize.width + 2 * .horizontalInset, height: textSize.height + 2 * .verticalInset)
    }
    
    func update(with symbolName: String) {
        textLabel.text = symbolName
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundView.frame = bounds
        
        let size = textLabel.sizeThatFits(backgroundView.bounds.size)
        let origin = CGPoint(
            x: backgroundView.frame.midX - size.width / 2,
            y: backgroundView.frame.midY - size.height / 2
        )
        textLabel.frame = CGRect(origin: origin, size: size)
    }
    
}

extension CGFloat {
    
    static let horizontalInset: CGFloat = 8
    static let verticalInset: CGFloat = 2
    
}
