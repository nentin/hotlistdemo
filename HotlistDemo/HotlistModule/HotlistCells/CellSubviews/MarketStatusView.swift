//
//  MarketStatusView.swift
//  HotlistDemo
//
//  Created by Nikita Entin on 29.08.2023.
//

import UIKit

final class MarketStatusView: UIView {
    
    private let stackView = UIStackView()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        addSubview(stackView)
        stackView.axis = .horizontal
        stackView.spacing = .spacing
        stackView.distribution = .equalSpacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let views = stackView.arrangedSubviews
        let width = views.map(\.frame.width).reduce(0, +) + stackView.spacing
        let height = views.map(\.frame.height).first ?? .zero
        return CGSize(width: width, height: height)
    }
    
    func update(with info: [String]) {
        info.forEach { imageName in
            let view = UIImageView(image: UIImage(named: imageName))
            stackView.addArrangedSubview(view)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        stackView.frame = bounds
    }
    
}

private extension CGFloat {
    
    static let spacing: CGFloat = 4
    
}
