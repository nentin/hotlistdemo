//
//  PriceChangeView.swift
//  HotlistDemo
//
//  Created by Nikita Entin on 31.08.2023.
//

import UIKit

final class PriceChangeView: UIView {
    
    private let changeLabel = UILabel()
    private let skeletonView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupInitialState()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let changeSize = changeLabel.sizeThatFits(size)
        if changeLabel.text == nil {
            return .skeleton
        } else {
            return changeSize
        }
    }
    
    func update(with priceChange: String?) {
        if priceChange?.isEmpty == false {
            changeLabel.text = priceChange
            changeLabel.isHidden = false
            skeletonView.isHidden = true
        } else {
            changeLabel.isHidden = true
            skeletonView.isHidden = false
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let changeSize = changeLabel.sizeThatFits(bounds.size)
        changeLabel.frame = CGRect(origin: .zero, size: changeSize)
        
        skeletonView.frame = CGRect(origin: .zero, size: .skeleton)
    }
    
    private func setupInitialState() {
        addSubview(changeLabel)
        addSubview(skeletonView)
        changeLabel.numberOfLines = 1
        changeLabel.textColor = .systemRed
        changeLabel.font = .preferredFont(forTextStyle: .subheadline)
        skeletonView.isHidden = true
        skeletonView.backgroundColor = .coldGray100
        skeletonView.clipsToBounds = true
        skeletonView.layer.cornerRadius = 4
    }
    
}

private extension CGSize {
    
    static let skeleton = CGSize(width: 56, height: 20)
    
}
