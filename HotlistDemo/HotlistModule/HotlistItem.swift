//
//  HotlistItem.swift
//  HotlistDemo
//
//  Created by Nikita Entin on 29.08.2023.
//

import Foundation

struct HotlistItem: Equatable {
    
    let symbol: String?
    let logo: String?
    let ticker: String?
    let price: String?
    let priceChange: String?
    let premarketStatus: [String]?
    let marketCap: String?
    
}
