//
//  MainCollectionCell.swift
//  HotlistDemo
//
//  Created by Nikita Entin on 30.08.2023.
//

import UIKit

final class MainCollectionCell: UICollectionViewCell {
    
    weak var delegate: CollectionCellControllerDelegate?
    
    private let controller = CollectionCellController()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        controller.delegate = self
        contentView.addSubview(controller.view)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        controller.view.frame = contentView.bounds
    }
    
    func prepare(with items: [HotlistItem], parent: UIViewController) {
        if !parent.contains(controller) {
            parent.addChild(controller)
            controller.didMove(toParent: parent)
        }
        controller.updateState(with: items)
    }
    
    func willDisplay() {
        // onAppear
    }
    
    func didEndDisplaying() {
        // onDisappear
    }
    
}

extension MainCollectionCell: CollectionCellControllerDelegate {
    
    func didSelectItem(at indexPath: IndexPath) {
        delegate?.didSelectItem(at: indexPath)
    }
    
}
