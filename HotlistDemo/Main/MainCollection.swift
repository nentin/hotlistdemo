//
//  MainCollection.swift
//  HotlistDemo
//
//  Created by Nikita Entin on 30.08.2023.
//

import UIKit
import SwiftUI

final class MainCollection: UIViewController {
    
    private lazy var collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
    
    private lazy var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = .zero
        return layout
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collection.backgroundColor = .systemRed
        collection.register(MainCollectionCell.self, forCellWithReuseIdentifier: .main)
        collection.dataSource = self
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "Main Collection"
        view.addSubview(collection)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collection.frame = view.bounds
        
        layout.itemSize = CGSize(width: view.bounds.width, height: 60 * 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? MainCollectionCell else { return }
        cell.willDisplay()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? MainCollectionCell else { return }
        cell.didEndDisplaying()
    }
    
}

extension MainCollection: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: .main, for: indexPath) as? MainCollectionCell else {
            fatalError()
        }
        cell.delegate = self
        cell.prepare(with: Self.makeItems(), parent: self)
        return cell
    }
    
}

extension MainCollection: CollectionCellControllerDelegate {
    
    func didSelectItem(at indexPath: IndexPath) {
        print(#function)
    }
    
}

private extension String {
    
    static let main = String(describing: MainCollectionCell.self)
    
}

private extension MainCollection {
    
    static func makeItems() -> [HotlistItem] {
        return [
            .init(symbol: "AAPL Inc.", logo: "aapl", ticker: "AAPL", price: "123.32", priceChange: "-213.3%", premarketStatus: ["premarket"], marketCap: nil),
            .init(symbol: "AAPL Inc.", logo: "aapl", ticker: "AAPL", price: "123.32", priceChange: "-213.3%", premarketStatus: [], marketCap: nil),
            .init(symbol: "AAPL Inc.", logo: "aapl", ticker: "AAPL", price: "123.32", priceChange: "-213.3%", premarketStatus: [], marketCap: nil),
            .init(symbol: "AAPL Inc.", logo: "aapl", ticker: "AAPL", price: "123.32", priceChange: "-213.3%", premarketStatus: [], marketCap: nil),
            .init(symbol: "AAPL Inc.", logo: "aapl", ticker: "AAPL", price: "123.32", priceChange: "-213.3%", premarketStatus: [], marketCap: nil),
            .init(symbol: "AAPL Inc.", logo: "aapl", ticker: "AAPL", price: "123.32", priceChange: "-213.3%", premarketStatus: [], marketCap: nil),
        ]
    }
    
    static func makeItemsWithMarketCap() -> [HotlistItem] {
        return [
            .init(symbol: "AAPL Inc.", logo: "aapl", ticker: "AAPL", price: "123.32", priceChange: "-213.3%", premarketStatus: [], marketCap: "Market cap: 838.56B USD"),
            .init(symbol: "AAPL Inc.", logo: "aapl", ticker: "AAPL", price: "123.32", priceChange: "-213.3%", premarketStatus: [], marketCap: "Market cap: 838.56B USD"),
            .init(symbol: "AAPL Inc.", logo: "aapl", ticker: "AAPL", price: "123.32", priceChange: "-213.3%", premarketStatus: [], marketCap: "Market cap: 838.56B USD"),
            .init(symbol: "AAPL Inc.", logo: "aapl", ticker: "AAPL", price: "123.32", priceChange: "-213.3%", premarketStatus: [], marketCap: "Market cap: 838.56B USD"),
            .init(symbol: "AAPL Inc.", logo: "aapl", ticker: "AAPL", price: "123.32", priceChange: "-213.3%", premarketStatus: [], marketCap: "Market cap: 838.56B USD"),
            .init(symbol: "AAPL Inc.", logo: "aapl", ticker: "AAPL", price: "123.32", priceChange: "-213.3%", premarketStatus: [], marketCap: "Market cap: 838.56B USD"),
        ]
    }
    
}
